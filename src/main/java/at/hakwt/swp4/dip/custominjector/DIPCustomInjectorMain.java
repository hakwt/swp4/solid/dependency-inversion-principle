package at.hakwt.swp4.dip.custominjector;

import at.hakwt.swp4.dip.*;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

public class DIPCustomInjectorMain {

    public static void main(String[] args) throws InvocationTargetException, NoSuchMethodException, InstantiationException, IllegalAccessException {
        DependencyInjector dependencyInjector = new DependencyInjector();

        BusinessProcess customerImportProcess = new ConfigureableBusinessProcess();
        List<BusinessProcessStep> services = new ArrayList<>();

        services.add(dependencyInjector.createInstanceOf(LoadFromCsvStep.class));
        services.add(dependencyInjector.createInstanceOf(DataCanBeStoredStep.class));
        services.add(dependencyInjector.createInstanceOf(StoreDataStep.class));

        customerImportProcess.configure(services);
        customerImportProcess.run();
    }


}
