package at.hakwt.swp4.dip;

import java.util.List;

/**
 * A business process in our application consists of a number of steps
 * that have to be performed. Such "steps" must implement the
 * {@link BusinessProcessStep} interface.
 *
 * An instance of a business process can be executed by invoking its
 * {@link #run()} method. Initially it can be externally configured
 * by using its {@link #configure(List)} method.
 *
 */
public interface BusinessProcess {

    /**
     * executes the process be running the configured business process
     * in the order they are configured.
     */
    void run();

    /**
     * Set the services of which the business process consists
     *
     * @param steps The concrete steps
     */
    void configure(List<BusinessProcessStep> steps);
}
